﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MyMSIS.Models
{
    public class PlanTermRequirement
    {
        public PlanTermRequirement()
        {
            RequirementStatusId = 1; // default to 1
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PlanTermRequirementId { get; internal set; }

        public int PlanTermId { get; internal set; }

        [Display(Name = "Term Number (used for sorting)")]
        public int TermNumber { get; internal set; }

        [Display(Name = "Requirement Number (used for sorting)")]
        public int RequirementNumber { get; internal set; }

        public int RequirementStatusId { get; internal set; }

        // Add navigation property for each related entity

        // each plan term requirement points to exactly one plan term
        public PlanTerm PlanTerm { get; set; }

        // each plan term requirement points to exactly one requirement status - have we earned credit in this term or not?
        public RequirementStatus RequirementStatus { get; set; }

        public override string ToString()
        {
            return base.ToString() + ": " +
              "PlanTermRequirementId = " + PlanTermRequirementId +
              "PlanTermId = " + PlanTermId +
              ", TermNumber = " + TermNumber +
              ", RequirementNumber = " + RequirementNumber +
              ", RequirementStatusId = " + RequirementStatusId +
              ", RequirementStatus: {" + RequirementStatus.ToString() +
                            "}, PlanTerm = {" + PlanTerm.ToString() +
                           "}";
        }
    }
}
