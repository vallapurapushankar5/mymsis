﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyMSIS.Data;
using MyMSIS.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MyMSIS.Controllers
{
    [Route("api/student")]
    public class StudentValuesController : Controller
    {
        private readonly MSISContext _context;

        public StudentValuesController(MSISContext context)
        {
            _context = context;

            if (_context.Students.Count() == 0)
            {
                _context.Students.Add(new Student { StudentId = 1, FamilyName = "Doe", GivenName = "John"});
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public IEnumerable<Student> GetAll()
        {
            return _context.Students.ToList();
        }

        [HttpGet("{id}", Name = "Student")]
        public IActionResult GetById(long id)
        {
            var item = _context.Students.FirstOrDefault(t => t.StudentId == id);
            
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Student s)
        {
            if (s == null)
            {
                return BadRequest();
            }

            _context.Students.Add(s);
            _context.SaveChanges();

            return CreatedAtRoute("GetStudent", new { id = s.StudentId }, s);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Student item)
        {
            if (item == null || item.StudentId != id)
            {
                return BadRequest();
            }

            var student = _context.Students.FirstOrDefault(t => t.StudentId== id);
            if (student == null)
            {
                return NotFound();
            }

            student.FamilyName = item.FamilyName;
            student.GivenName = item.GivenName;

            _context.Students.Update(student);
            _context.SaveChanges();
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long StudentId)
        {
            var student = _context.Students.FirstOrDefault(t => t.StudentId == StudentId);
            if (student == null)
            {
                return NotFound();
            }

            _context.Students.Remove(student);
            _context.SaveChanges();
            return new NoContentResult();
        }

    }
}
