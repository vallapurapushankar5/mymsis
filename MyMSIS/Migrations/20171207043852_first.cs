﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MyMSIS.Migrations
{
    public partial class first : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Degree",
                columns: table => new
                {
                    DegreeId = table.Column<int>(type: "int", nullable: false),
                    DegreeAbbrev = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    DegreeName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Degree", x => x.DegreeId);
                });

            migrationBuilder.CreateTable(
                name: "DegreeStatus",
                columns: table => new
                {
                    DegreeStatusId = table.Column<int>(type: "int", nullable: false),
                    Degree_Status = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DegreeStatus", x => x.DegreeStatusId);
                });

            migrationBuilder.CreateTable(
                name: "RequirementStatus",
                columns: table => new
                {
                    RequirementStatusId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequirementStatus", x => x.RequirementStatusId);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    FamilyName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    GivenName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.StudentId);
                });

            migrationBuilder.CreateTable(
                name: "DegreeRequirement",
                columns: table => new
                {
                    DegreeRequirementId = table.Column<int>(type: "int", nullable: false),
                    DegreeId = table.Column<int>(type: "int", nullable: false),
                    RequirementAbbrev = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    RequirementName = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: false),
                    RequirementNumber = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DegreeRequirement", x => x.DegreeRequirementId);
                    table.ForeignKey(
                        name: "FK_DegreeRequirement_Degree_DegreeId",
                        column: x => x.DegreeId,
                        principalTable: "Degree",
                        principalColumn: "DegreeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentDegreePlan",
                columns: table => new
                {
                    StudentDegreePlanId = table.Column<int>(type: "int", nullable: false),
                    DegreeId = table.Column<int>(type: "int", nullable: false),
                    DegreeStatusId = table.Column<int>(type: "int", nullable: false),
                    PlanAbbrev = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    PlanName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PlanNumber = table.Column<int>(type: "int", nullable: false),
                    StudentId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentDegreePlan", x => x.StudentDegreePlanId);
                    table.ForeignKey(
                        name: "FK_StudentDegreePlan_Degree_DegreeId",
                        column: x => x.DegreeId,
                        principalTable: "Degree",
                        principalColumn: "DegreeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentDegreePlan_DegreeStatus_DegreeStatusId",
                        column: x => x.DegreeStatusId,
                        principalTable: "DegreeStatus",
                        principalColumn: "DegreeStatusId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentDegreePlan_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "StudentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanTerm",
                columns: table => new
                {
                    PlanTermId = table.Column<int>(type: "int", nullable: false),
                    StudentDegreePlanId = table.Column<int>(type: "int", nullable: false),
                    TermAbbrev = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    TermNumber = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanTerm", x => x.PlanTermId);
                    table.ForeignKey(
                        name: "FK_PlanTerm_StudentDegreePlan_StudentDegreePlanId",
                        column: x => x.StudentDegreePlanId,
                        principalTable: "StudentDegreePlan",
                        principalColumn: "StudentDegreePlanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanTermRequirement",
                columns: table => new
                {
                    PlanTermRequirementId = table.Column<int>(type: "int", nullable: false),
                    PlanTermId = table.Column<int>(type: "int", nullable: false),
                    RequirementNumber = table.Column<int>(type: "int", nullable: false),
                    RequirementStatusId = table.Column<int>(type: "int", nullable: false),
                    TermNumber = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanTermRequirement", x => x.PlanTermRequirementId);
                    table.ForeignKey(
                        name: "FK_PlanTermRequirement_PlanTerm_PlanTermId",
                        column: x => x.PlanTermId,
                        principalTable: "PlanTerm",
                        principalColumn: "PlanTermId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanTermRequirement_RequirementStatus_RequirementStatusId",
                        column: x => x.RequirementStatusId,
                        principalTable: "RequirementStatus",
                        principalColumn: "RequirementStatusId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DegreeRequirement_DegreeId",
                table: "DegreeRequirement",
                column: "DegreeId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanTerm_StudentDegreePlanId",
                table: "PlanTerm",
                column: "StudentDegreePlanId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanTermRequirement_PlanTermId",
                table: "PlanTermRequirement",
                column: "PlanTermId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanTermRequirement_RequirementStatusId",
                table: "PlanTermRequirement",
                column: "RequirementStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlan_DegreeId",
                table: "StudentDegreePlan",
                column: "DegreeId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlan_DegreeStatusId",
                table: "StudentDegreePlan",
                column: "DegreeStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlan_StudentId",
                table: "StudentDegreePlan",
                column: "StudentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DegreeRequirement");

            migrationBuilder.DropTable(
                name: "PlanTermRequirement");

            migrationBuilder.DropTable(
                name: "PlanTerm");

            migrationBuilder.DropTable(
                name: "RequirementStatus");

            migrationBuilder.DropTable(
                name: "StudentDegreePlan");

            migrationBuilder.DropTable(
                name: "Degree");

            migrationBuilder.DropTable(
                name: "DegreeStatus");

            migrationBuilder.DropTable(
                name: "Student");
        }
    }
}
