﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MyMSIS.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "StudentDegreePlan");

            migrationBuilder.DropColumn(
                name: "EditDate",
                table: "StudentDegreePlan");

            migrationBuilder.DropColumn(
                name: "Requirement_Status",
                table: "RequirementStatus");

            migrationBuilder.DropColumn(
                name: "DegreeId",
                table: "PlanTermRequirement");

            migrationBuilder.DropColumn(
                name: "PlanNumber",
                table: "PlanTermRequirement");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "PlanTermRequirement");

            migrationBuilder.DropColumn(
                name: "DegreeId",
                table: "PlanTerm");

            migrationBuilder.DropColumn(
                name: "PlanNumber",
                table: "PlanTerm");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "PlanTerm");

            migrationBuilder.DropColumn(
                name: "Degree_Status",
                table: "DegreeStatus");

            migrationBuilder.DropColumn(
                name: "lkDegree",
                table: "DegreeRequirement");

            migrationBuilder.AlterColumn<string>(
                name: "PlanName",
                table: "StudentDegreePlan",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "PlanAbbrev",
                table: "StudentDegreePlan",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AddColumn<bool>(
                name: "IncludesInternship",
                table: "StudentDegreePlan",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<string>(
                name: "FamilyName",
                table: "Student",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "RequirementStatus",
                type: "nvarchar(15)",
                maxLength: 15,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "PlanTermId",
                table: "PlanTermRequirement",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "TermAbbrev",
                table: "PlanTerm",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.AddColumn<int>(
                name: "StudentDegreePlanId",
                table: "PlanTerm",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "DegreeStatus",
                type: "nvarchar(15)",
                maxLength: 15,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "RequirementName",
                table: "DegreeRequirement",
                type: "nvarchar(60)",
                maxLength: 60,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "RequirementAbbrev",
                table: "DegreeRequirement",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.AlterColumn<string>(
                name: "DegreeName",
                table: "Degree",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 150);

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlan_DegreeId",
                table: "StudentDegreePlan",
                column: "DegreeId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlan_DegreeStatusId",
                table: "StudentDegreePlan",
                column: "DegreeStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlan_StudentId",
                table: "StudentDegreePlan",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanTermRequirement_PlanTermId",
                table: "PlanTermRequirement",
                column: "PlanTermId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanTermRequirement_RequirementStatusId",
                table: "PlanTermRequirement",
                column: "RequirementStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanTerm_StudentDegreePlanId",
                table: "PlanTerm",
                column: "StudentDegreePlanId");

            migrationBuilder.CreateIndex(
                name: "IX_DegreeRequirement_DegreeId",
                table: "DegreeRequirement",
                column: "DegreeId");

            migrationBuilder.AddForeignKey(
                name: "FK_DegreeRequirement_Degree_DegreeId",
                table: "DegreeRequirement",
                column: "DegreeId",
                principalTable: "Degree",
                principalColumn: "DegreeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlanTerm_StudentDegreePlan_StudentDegreePlanId",
                table: "PlanTerm",
                column: "StudentDegreePlanId",
                principalTable: "StudentDegreePlan",
                principalColumn: "StudentDegreePlanId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlanTermRequirement_PlanTerm_PlanTermId",
                table: "PlanTermRequirement",
                column: "PlanTermId",
                principalTable: "PlanTerm",
                principalColumn: "PlanTermId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlanTermRequirement_RequirementStatus_RequirementStatusId",
                table: "PlanTermRequirement",
                column: "RequirementStatusId",
                principalTable: "RequirementStatus",
                principalColumn: "RequirementStatusId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentDegreePlan_Degree_DegreeId",
                table: "StudentDegreePlan",
                column: "DegreeId",
                principalTable: "Degree",
                principalColumn: "DegreeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentDegreePlan_DegreeStatus_DegreeStatusId",
                table: "StudentDegreePlan",
                column: "DegreeStatusId",
                principalTable: "DegreeStatus",
                principalColumn: "DegreeStatusId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentDegreePlan_Student_StudentId",
                table: "StudentDegreePlan",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "StudentId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DegreeRequirement_Degree_DegreeId",
                table: "DegreeRequirement");

            migrationBuilder.DropForeignKey(
                name: "FK_PlanTerm_StudentDegreePlan_StudentDegreePlanId",
                table: "PlanTerm");

            migrationBuilder.DropForeignKey(
                name: "FK_PlanTermRequirement_PlanTerm_PlanTermId",
                table: "PlanTermRequirement");

            migrationBuilder.DropForeignKey(
                name: "FK_PlanTermRequirement_RequirementStatus_RequirementStatusId",
                table: "PlanTermRequirement");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentDegreePlan_Degree_DegreeId",
                table: "StudentDegreePlan");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentDegreePlan_DegreeStatus_DegreeStatusId",
                table: "StudentDegreePlan");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentDegreePlan_Student_StudentId",
                table: "StudentDegreePlan");

            migrationBuilder.DropIndex(
                name: "IX_StudentDegreePlan_DegreeId",
                table: "StudentDegreePlan");

            migrationBuilder.DropIndex(
                name: "IX_StudentDegreePlan_DegreeStatusId",
                table: "StudentDegreePlan");

            migrationBuilder.DropIndex(
                name: "IX_StudentDegreePlan_StudentId",
                table: "StudentDegreePlan");

            migrationBuilder.DropIndex(
                name: "IX_PlanTermRequirement_PlanTermId",
                table: "PlanTermRequirement");

            migrationBuilder.DropIndex(
                name: "IX_PlanTermRequirement_RequirementStatusId",
                table: "PlanTermRequirement");

            migrationBuilder.DropIndex(
                name: "IX_PlanTerm_StudentDegreePlanId",
                table: "PlanTerm");

            migrationBuilder.DropIndex(
                name: "IX_DegreeRequirement_DegreeId",
                table: "DegreeRequirement");

            migrationBuilder.DropColumn(
                name: "IncludesInternship",
                table: "StudentDegreePlan");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "RequirementStatus");

            migrationBuilder.DropColumn(
                name: "PlanTermId",
                table: "PlanTermRequirement");

            migrationBuilder.DropColumn(
                name: "StudentDegreePlanId",
                table: "PlanTerm");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "DegreeStatus");

            migrationBuilder.AlterColumn<string>(
                name: "PlanName",
                table: "StudentDegreePlan",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "PlanAbbrev",
                table: "StudentDegreePlan",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "StudentDegreePlan",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "EditDate",
                table: "StudentDegreePlan",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<string>(
                name: "FamilyName",
                table: "Student",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Requirement_Status",
                table: "RequirementStatus",
                maxLength: 25,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "DegreeId",
                table: "PlanTermRequirement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PlanNumber",
                table: "PlanTermRequirement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "PlanTermRequirement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "TermAbbrev",
                table: "PlanTerm",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DegreeId",
                table: "PlanTerm",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PlanNumber",
                table: "PlanTerm",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "PlanTerm",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Degree_Status",
                table: "DegreeStatus",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "RequirementName",
                table: "DegreeRequirement",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(60)",
                oldMaxLength: 60);

            migrationBuilder.AlterColumn<string>(
                name: "RequirementAbbrev",
                table: "DegreeRequirement",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldMaxLength: 10);

            migrationBuilder.AddColumn<string>(
                name: "lkDegree",
                table: "DegreeRequirement",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "DegreeName",
                table: "Degree",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);
        }
    }
}
