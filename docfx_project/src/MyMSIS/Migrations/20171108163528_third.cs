﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MyMSIS.Migrations
{
    public partial class third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "DegreeStatus");

            migrationBuilder.AddColumn<string>(
                name: "Degree_Status",
                table: "DegreeStatus",
                type: "nvarchar(15)",
                maxLength: 15,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Degree_Status",
                table: "DegreeStatus");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "DegreeStatus",
                maxLength: 15,
                nullable: false,
                defaultValue: "");
        }
    }
}
