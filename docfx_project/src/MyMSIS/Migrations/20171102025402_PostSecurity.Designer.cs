﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using MyMSIS.Data;
using System;

namespace MyMSIS.Migrations
{
    [DbContext(typeof(MSISContext))]
    [Migration("20171102025402_PostSecurity")]
    partial class PostSecurity
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MyMSIS.Models.Degree", b =>
                {
                    b.Property<int>("DegreeId");

                    b.Property<string>("DegreeAbbrev")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("DegreeName")
                        .IsRequired()
                        .HasMaxLength(150);

                    b.HasKey("DegreeId");

                    b.ToTable("Degree");
                });

            modelBuilder.Entity("MyMSIS.Models.DegreeRequirement", b =>
                {
                    b.Property<int>("DegreeRequirementId");

                    b.Property<int>("DegreeId");

                    b.Property<string>("RequirementAbbrev")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("RequirementName")
                        .IsRequired()
                        .HasMaxLength(150);

                    b.Property<int>("RequirementNumber");

                    b.Property<string>("lkDegree")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.HasKey("DegreeRequirementId");

                    b.ToTable("DegreeRequirement");
                });

            modelBuilder.Entity("MyMSIS.Models.DegreeStatus", b =>
                {
                    b.Property<int>("DegreeStatusId");

                    b.Property<string>("Degree_Status")
                        .HasMaxLength(100);

                    b.HasKey("DegreeStatusId");

                    b.ToTable("DegreeStatus");
                });

            modelBuilder.Entity("MyMSIS.Models.PlanTerm", b =>
                {
                    b.Property<int>("PlanTermId");

                    b.Property<int>("DegreeId");

                    b.Property<int>("PlanNumber");

                    b.Property<int>("StudentId");

                    b.Property<string>("TermAbbrev")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<int>("TermNumber");

                    b.HasKey("PlanTermId");

                    b.ToTable("PlanTerm");
                });

            modelBuilder.Entity("MyMSIS.Models.PlanTermRequirement", b =>
                {
                    b.Property<int>("PlanTermRequirementId");

                    b.Property<int>("DegreeId");

                    b.Property<int>("PlanNumber");

                    b.Property<int>("RequirementNumber");

                    b.Property<int>("RequirementStatusId");

                    b.Property<int>("StudentId");

                    b.Property<int>("TermNumber");

                    b.HasKey("PlanTermRequirementId");

                    b.ToTable("PlanTermRequirement");
                });

            modelBuilder.Entity("MyMSIS.Models.RequirementStatus", b =>
                {
                    b.Property<int>("RequirementStatusId");

                    b.Property<string>("Requirement_Status")
                        .IsRequired()
                        .HasMaxLength(25);

                    b.HasKey("RequirementStatusId");

                    b.ToTable("RequirementStatus");
                });

            modelBuilder.Entity("MyMSIS.Models.Student", b =>
                {
                    b.Property<int>("StudentId");

                    b.Property<string>("FamilyName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("GivenName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("StudentId");

                    b.ToTable("Student");
                });

            modelBuilder.Entity("MyMSIS.Models.StudentDegreePlan", b =>
                {
                    b.Property<int>("StudentDegreePlanId");

                    b.Property<DateTime>("CreateDate");

                    b.Property<int>("DegreeId");

                    b.Property<int>("DegreeStatusId");

                    b.Property<DateTime>("EditDate");

                    b.Property<string>("PlanAbbrev")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("PlanName")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("PlanNumber");

                    b.Property<int>("StudentId");

                    b.HasKey("StudentDegreePlanId");

                    b.ToTable("StudentDegreePlan");
                });
#pragma warning restore 612, 618
        }
    }
}
