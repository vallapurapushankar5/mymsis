﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Xunit;
using MyMSIS.Models;

namespace MyMSIS.Tests.Models
{
    public class DegreeTest
    {
        [Fact]
        public void CreatesDegreeWithGivenInformation()
        {
            // Arrange
            var model = new Degree
            {
                DegreeId = 123,
                DegreeName = "Test",
                DegreeAbbrev = "Test"
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.True(success);
        }

        [Fact]
        public void Should_Not_Create_Degree_Without_DegreeAbbrev()
        {
            // Arrange
            var model = new Degree
            {
                DegreeId = 123,
                DegreeName = "Test"
            };
            var validationResults = new List<ValidationResult>();

            //Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success);  // should not be successful
        }


        [Fact]
        public void Should_Not_Create_Degree_Without_DegreeName()
        {
            // Arrange
            var model = new Degree
            {
                DegreeId = 123,
                DegreeAbbrev = "Test"
            };
            var validationResults = new List<ValidationResult>();

            //Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success); // should not be successful
        }



        [Fact]
        public void Degree_abbrev_must_be_short()
        {
            // Arrange
            var model = new Degree()
            {
                DegreeId = 1234,
                DegreeAbbrev = "123456789012345678901234567890123456789012345678901234567890",
                DegreeName = "Test"
            };
            var context = new ValidationContext(model, null, null);
            var result = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, context, result, true);

            // Assert
            Assert.False(success);
            var failure = Assert.Single(
                result,
                x => x.ErrorMessage == "Abbreviation cannot be longer than 10 characters.");
            Assert.Single(failure.MemberNames, x => x == "DegreeAbbrev");
        }

        [Fact]
        public void Degree_name_must_be_short()
        {
            // Arrange
            var model = new Degree()
            {
                DegreeId = 1234,
                DegreeAbbrev = "Test",
                DegreeName = "123456789012345678901234567890123456789012345678901234567890"
            };
            var context = new ValidationContext(model, null, null);
            var result = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, context, result, true);

            // Assert
            Assert.False(success);
            var failure = Assert.Single(
                result,
                x => x.ErrorMessage == "Name cannot be longer than 50 characters.");
            Assert.Single(failure.MemberNames, x => x == "DegreeName");
        }

    }
}
