using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace MyMSIS.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {

        }

        //Matt's Test
        [Fact]
        public void CreatesStudentWithGivenInformation()
        {
            //Arrange

            // new Student { StudentId = 1531, GivenName = "Peanut", FamilyName = "McNubbin" },
            var model = new MyMSIS.Models.Student
            {
                StudentId = 1531 ,
                GivenName = "Peanut",
                FamilyName = "McNubbin"
            };
            //Act
            var validationResults = new List<ValidationResult>();
           
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            //Assert

            Assert.True(success);
        }
        [Fact]
        public void Should_Not_Create_Student_Without_GivenName()
        {
            // Arrange
            // This should not work since it does not have a given name
            var model = new MyMSIS.Models.Student
            {
                StudentId = 123,
                FamilyName = "Test"
                
            };
            var context = new ValidationContext(model, null, null);
            var validationResults = new List<ValidationResult>();

            //Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success); // should not be successful
            var failure = Assert.Single(validationResults);
            Assert.Single(failure.MemberNames, x => x == "GivenName");

        }
		 // Mounika Test case
        [Fact]
        public void Should_Not_Create_Student_Without_FamilyName()
        {
            // Arrange
            // This should not work since it doesn't have a family name
            var model = new MyMSIS.Models.Student
            {
                StudentId = 123,
                GivenName = "Test"

            };
            var context = new ValidationContext(model, null, null);
            var validationResults = new List<ValidationResult>();

            //Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success); // should not be successful
            var failure = Assert.Single(validationResults);
            Assert.Single(failure.MemberNames, x => x == "GivenName");

        }

    }
}
